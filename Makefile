CC=g++
OPTIMIZATION=-O3
CFLAGS=-ansi -std=c++0x -pedantic -Iincludes $(OPTIMIZATION)
CFLAGS_DEBUG=-W -g -Wall -ansi -std=c++11  -pedantic -Iincludes -O0 -check-uninit -debug all
CFLAGS_MIC=$(CFLAGS) -mmic
LDFLAGS=-O3
LDFLAGS_DEBUG=-openmp -g -O0 -vec-report=2 -check-uninit -debug all
LDFLAGS_MIC=-openmp -g $(OPTIMIZATION) -mmic
EXEC=prog 
SRC=$(wildcard src/*.cpp)
OBJ=$(SRC:src/%.cpp=obj/%.o)
OBJ_MIC=$(SRC:src/%.cpp=obj/%.omic)
OBJ_DEBUG=$(SRC:src/%.cpp=obj/%.odeb)
TEST=test_case_1

all:$(EXEC)

mic:$(OBJ_MIC)
	$(CC) -o $@ $^ $(LDFLAGS_MIC)

default:all

debug:$(OBJ_DEBUG)
	$(CC) -o $@ $^ $(LDFLAGS_DEBUG)

prog:$(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

run:
	./$(EXEC) 4 4 $(TEST)/img2.bmp $(TEST)/001template.bmp $(TEST)/002template2.bmp

obj/%.o:src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

obj/%.omic:src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS_MIC)

obj/%.odeb:src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS_DEBUG)

clean:
	rm -f obj/*.o* $(EXEC) *.zip debug

