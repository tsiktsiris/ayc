#include <iostream>
#include "BitmapImporter.h"
#include <omp.h>
#include <string>
#include <list>

using namespace std;

/*!
 *\struct Parameters
 *\brief This structure holds the application parameters
 */
typedef struct
{
	int nb_threads;
	string main_image_name;
	list<std::string> template_names;
	int max_scale;
}Parameters;

/*!
 *\brief This structure hold a single result
 */
typedef struct
{
	int pattern_ID;
	int position_x;
	int position_y;
}Result;

/*!
 * \brief Try to match the exact template in the main image starting at coordinates [h,w] in the main image.
 * \param main_image The main image.
 * \param template_image The template.
 * \param h The row number.
 * \param w the coloumn number.
 * \return template has been found at position [h,w] in the main image.  
 */
bool match_template(const Accelerate::Image& main_image, const Accelerate::Image& template_image, unsigned int h, unsigned int w);

/*!
 * \brief Read the parameters
 * \param argc The number of parameters (including the program call).
 * \param argv The array of parameters.
 * \return The parameters have been read correctly.
 */
bool read_parameters(int argc, char* argv[], Parameters& parameters);


bool compare_results(Result& first, Result& second)
{
	if(first.pattern_ID < second.pattern_ID) return true;
	else if(first.pattern_ID > second.pattern_ID) return false;

	if(first.position_x < second.position_x) return true;
	else if(first.position_x > second.position_x) return false;

	if(first.position_y < second.position_y) return true;
	else if(first.position_y > second.position_y) return false;

	return true;
}

bool read_parameters(int argc, char* argv[], Parameters& parameters)
{
	if(argc < 4) return false;

	parameters.nb_threads = atoi(argv[1]);
	if(parameters.nb_threads < 0) return false;

	parameters.max_scale = atoi(argv[2]);
	if(parameters.max_scale <= 0) return false;

	parameters.main_image_name = string(argv[3]);

	for(unsigned int i=4; i<argc; i++)
		parameters.template_names.push_back(string(argv[i]));

	return true;
}

int main(int argc, char* argv[]){
	if(argc<=1) return 0;
	if(argv == NULL) return 0;

	Parameters parameters;
	list<Result> result_list;
	if(!read_parameters(argc, argv, parameters))
	{
		cout<<"Wrong number of parameters or invalid parameters..."<<endl;
		cout<<"The program must be called with the following parameters:"<<endl;
		cout<<"\t- num_threads: The number of threads"<<endl;
		cout<<"\t- max_scale: The maximum scale that can be applied to the templates in the main image"<<endl;
		cout<<"\t- main_image: The main image path"<<endl;
		cout<<"\t- t1 ... tn: The list of the template paths. Each template separated by a space"<<endl;
		cout<<endl<<"For example : ./run 4 3 img.bmp template1.bmp template2.bmp"<<endl;
		return -1;
	}

	//Read the main image
	Accelerate::Image main_image = Accelerate::Image::create_image_from_bitmap(parameters.main_image_name);
	//iterates over the pattern images
	for(string temp_name : parameters.template_names)
	{
		//Read a specific pattern image
		Accelerate::Image template_image = Accelerate::Image::create_image_from_bitmap(temp_name);
		//Then retrieve the ID of the image (the 3 first characters
		int template_id = atoi(temp_name.substr(0, 3).c_str());	
		//Iterate over some possible scales (you can add more steps and you can also check rotations)
		//The sample is really simple because you have to create a good algorithm able to match
		//a pattern in an image
		for(unsigned int s=1; s<=parameters.max_scale; s++)
		{
			//Create a scaled image
			Accelerate::Image temp = template_image.scale_image(s);
			//iterates on the main image pixels
			for(unsigned int wm=0; wm<main_image.get_width(); wm++)
			{
				for(unsigned int hm=0; hm<main_image.get_height(); hm++)
				{
					//Try to match the template
					if(match_template(main_image, temp, hm, wm))
					{
						//The pattern image has been found so save the result
						Result result;
						result.pattern_ID = template_id;
						result.position_x = wm;
						result.position_y = hm;
						result_list.push_back(result);		
					}
				}
			}
		}
	}
	//sort the result list
	result_list.sort(compare_results);

	//Print the results
	for(Result res : result_list)
		cout<<res.pattern_ID<<"\t"<<res.position_x<<"\t"<<res.position_y<<endl;

	return 0;
}



bool match_template(const Accelerate::Image& main_image, const Accelerate::Image& template_image, unsigned int h, unsigned int w)
{
	//The next cases are not possible
	if(main_image.get_width() - w < template_image.get_width()) return false;
	if(main_image.get_height() - h < template_image.get_height()) return false;
	//Iterates over the pattern and compare each pixel with the pixels of the main image
	for(unsigned int wt=0; wt<template_image.get_width(); wt++){
		for(unsigned int ht=0; ht<template_image.get_height(); ht++){
			//If a single pixel do not match, we return false
			//You can also improve this part of the algorithm as some images can have different contrasts,
			//lights, etc
			if(template_image.get_pixel(ht, wt).r != main_image.get_pixel(h+(ht), w+(wt)).r ||
				template_image.get_pixel(ht, wt).g != main_image.get_pixel(h+(ht), w+(wt)).g || 
				template_image.get_pixel(ht, wt).b != main_image.get_pixel(h+(ht), w+(wt)).b)
			{ 
				return false;
			}
		}
	}
	return true;
}

